import { store } from '../store'

export default (to, from, next) => {
  if (store.getters.getUserState) {
    next()
  } else {
    next('/signIn')
  }
}
