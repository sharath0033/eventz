import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home'
import Eventz from '@/components/eventz/eventz'
import ViewEvent from '@/components/eventz/viewEvent'
import CreateEvent from '@/components/eventz/createEvent'
import Profile from '@/components/user/profile'
import SignIn from '@/components/user/signIn'
import SignUp from '@/components/user/signUp'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      beforeEnter: AuthGuard
    },
    {
      path: '/eventz',
      name: 'Eventz',
      component: Eventz,
      beforeEnter: AuthGuard
    },
    {
      path: '/create',
      name: 'CreateEvent',
      component: CreateEvent,
      beforeEnter: AuthGuard
    },
    {
      path: '/eventz/:id',
      name: 'ViewEvent',
      props: true,
      component: ViewEvent,
      beforeEnter: AuthGuard
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/signIn',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/signUp',
      name: 'SignUp',
      component: SignUp
    }
  ]
})
