import Vue from 'vue'
import Vuex from 'vuex'
import Router from '../router'
import * as firebase from 'firebase'
import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'

Vue.use(Vuex)

export const store = new Vuex.Store({
  plugins: [
    createPersistedState({
      getState: (key) => Cookies.getJSON(key),
      setState: (key, state) => Cookies.set(key, state, { expires: 3, secure: false })
    })
  ],
  state: {
    loadedEventz: [],
    user: null,
    loading: false,
    error: null
  },
  mutations: {
    setLoadedEventz (state, payload) {
      state.loadedEventz = payload
    },
    createEvent (state, payload) {
      state.loadedEventz.push(payload)
    },
    setUser (state, payload) {
      state.user = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setError (state, payload) {
      if (payload === false) {
        state.error = null
      } else {
        state.error = payload
      }
    }
  },
  actions: {
    loadEventz ({commit}) {
      commit('setLoading', true)
      firebase.database().ref('eventz').once('value')
      .then((data) => {
        const eventz = []
        const obj = data.val()
        for (let key in obj) {
          eventz.push({
            id: key,
            title: obj[key].title,
            location: obj[key].location,
            imageUrl: obj[key].imageUrl,
            description: obj[key].description,
            startDate: obj[key].startDate,
            startTime: obj[key].startTime,
            endDate: obj[key].endDate,
            endTime: obj[key].endTime,
            registeredCount: obj[key].registeredCount,
            createdUser: obj[key].createdUser
          })
        }
        commit('setLoadedEventz', eventz)
        commit('setLoading', false)
      })
      .catch((error) => {
        commit('setLoading', false)
        console.log(error)
      })
    },
    createEventPayload ({commit, state}, payload) {
      commit('setLoading', true)
      const event = {
        title: payload.title,
        location: payload.location,
        description: payload.description,
        imageUrl: payload.imageUrl,
        startDate: payload.startDate,
        startTime: payload.startTime,
        endDate: payload.endDate,
        endTime: payload.endTime,
        registeredCount: payload.registeredCount,
        createdUser: state.user.userId
      }
      let key
      if (payload.image === null) {
        firebase.database().ref('eventz').push(event)
          .then((data) => {
            key = data.key
            commit('createEvent', {
              ...event,
              id: key
            })
            commit('setLoading', false)
            return key
          })
          .catch((error) => {
            commit('setLoading', false)
            console.log(error)
          })
      } else {
        let imageUrl
        firebase.database().ref('eventz').push(event)
          .then((data) => {
            key = data.key
            return key
          })
          .then(key => {
            const filename = payload.image.name
            const extension = filename.slice(filename.lastIndexOf('.'))
            return firebase.storage().ref('eventz/' + key + '.' + extension).put(payload.image)
          })
          .then(fileData => {
            imageUrl = fileData.metadata.downloadURLs[0]
            return firebase.database().ref('eventz').child(key).update({imageUrl: imageUrl})
          })
          .then(() => {
            commit('createEvent', {
              ...event,
              imageUrl: imageUrl,
              id: key
            })
            commit('setLoading', false)
          })
          .catch((error) => {
            commit('setLoading', false)
            console.log(error)
          })
      }
    },
    signUserUp ({commit}, payload) {
      commit('setLoading', true)
      commit('setError', false)
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            commit('setLoading', false)
            const newUser = {
              userId: user.uid,
              registeredEventz: []
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            commit('setLoading', false)
            commit('setError', error)
            console.log(error)
          }
        )
    },
    signUserIn ({commit}, payload) {
      commit('setLoading', true)
      commit('setError', false)
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            commit('setLoading', false)
            const newUser = {
              userId: user.uid,
              registeredEventz: []
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            commit('setLoading', false)
            commit('setError', error)
            console.log(error)
          }
        )
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', {userId: payload.uid, registeredEventz: []})
    },
    signOutUser ({commit}) {
      firebase.auth().signOut()
        .then((data) => {
          Router.push('/signIn')
          commit('setUser', data)
        })
    }
  },
  getters: {
    loadedEventz (state) {
      return state.loadedEventz.sort((eventA, eventB) => {
        return eventA.registeredCount > eventB.registeredCount
      })
    },
    featuredEventz (state, getters) {
      return getters.loadedEventz.reverse().slice(0, 3)
    },
    selectedEvent (state) {
      return (eventId) => {
        return state.loadedEventz.find((event) => {
          return event.id === eventId
        })
      }
    },
    getUserState (state) {
      return state.user
    },
    getLoading (state) {
      return state.loading
    },
    getError (state) {
      return state.error
    }
  }
})
