import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import { store } from './store'

Vue.use(Vuetify)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyBKE0OdlUPslnOrZUEw52gYnUaJbJIONGI',
      authDomain: 'eventz-application.firebaseapp.com',
      databaseURL: 'https://eventz-application.firebaseio.com',
      projectId: 'eventz-application',
      storageBucket: 'gs://eventz-application.appspot.com'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
        this.$store.dispatch('loadEventz')
      }
    })
  }
})
